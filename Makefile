.PHONY: clean ea
clean:   ;
	@latexmk -silent -c > /dev/null&
	@rm -f *.acn *.acr *.alg *.bbl *.glg *.glo *.gls *.ist *.run.xml&
ag: ;
	@latexmk -lualatex -time -silent -pvc agproblems
galois: ;
	@latexmk -lualatex -time -silent -pvc galois
ea: ;
	@pandoc ea1.tex --standalone --katex --output ea1.html&
	@latexmk -lualatex -time -silent -pvc ea1

