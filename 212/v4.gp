/* Given roots, compute (sqaree root of) the discriminant */
r2d(v) = prod(j=2,#v,prod(i=1,j-1,v[i]-v[j]))
/* Given roots, compute the polynomial that annihilates them */
r2p(v) = prod(k=1,#v,x-v[k])
/* Let a^2 = A, b^2 = B. Next function changes back a,b\in L to A,BR*/
L2K(x) = substpol(substpol(x,a^2,A),b^2,B)
/* The primitive element of the composite L = K(sqrt(A),sqrt(B)) is a+b with its 3 conjugates */
V = [a+b,a-b,-a+b,-a-b]
/* The polynomial with its coefficients in terms of K */
P = L2K(r2p(V))
/* We see that discriminant r2d(V) already lies in K, so Disc(P) is a square in K */
D = poldisc(P)
d = L2K(r2d(V))
D == d^2
/* Residues of D mod 5 */
M = matrix(5,5,AA,BB,substvec(D,[A,B],[AA,BB]) %5)
/* Vejamos e.g. que A=1,B=2 %5 -> D=4(5) e A=1,B=3(5) -> D=1(1) */
