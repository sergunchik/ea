% arara: lualatex
\documentclass{scrlttr2}
\usepackage{import}
\import{../}{p}
\begin{document} 
\newcommand{\CL}{Chambert-Loir}
\emph{Discriminantes.}

Sejam $K$ --- um corpo base (e.g. $K=\Q$),
$L/K$ --- uma extensão separável de grau $[L:K] = n < ∞$.
Fixamos um fechamento algébrico $Ω \supset K$ (e.g. $\C \supset \Q$ ou $\overline{\Q}\supset\Q$).
Será suficiente considerar uma normalização/galoisação (um corpo de fatoração) de $L$ sobre $K$.

\begin{prop}[Teorema 3.1.6 no \CL] \label{p:raizes-mergulhações}
Pelo um polinômio irredutível $P\in K[x]$ de grau $n$,
as mergulhões do corpo-quociente $σ : K[x]/P \to Ω$
são em bijeção com as raízes de $P$ em $Ω$.
Um polinômio $P$ é separável se e só se ele tem $\deg P$ raízes em $Ω$.
\end{prop}
\begin{proof}
Se $σ : L \to Ω$ é um morfismo, então $σ(x)$ é uma raiz de $P$.
Vice versa, pela $y\in Ω$ tal que $P(y) = 0$, o morfismo $K[x] \to Ω$ que manda $x$ para $y$
tem $P$ no seu núcleo e determina o morfismo de anel quociente $K[x]/P$.
\end{proof}
Sejam $σ_1,…,σ_n : L \to Ω$ todas as mergulhões de $L$ no $Ω$.

O elemento $x\in L$ é chamado \emph{primitivo} se ele gera $L$ sobre $K$,
equivalentemente $L = K[y]/P_x$ onde $P_x$ é o polinômio mínimo de $x$,
ou $\deg_K(x) = [L:K]$ (aqui $\deg_K (x) := \deg P_x = [K(x):K]$).

\begin{teorema}[Teorema sobre um elemento primitivo. Teorema 3.3.3 no \CL ou 23.12 na AATA]
Qualquer extensão separável finita $L/K$ tem os elementos primitivos.
\end{teorema}

\emph{O traço} é uma aplicação $K$-linear $\tr_{L/K} : L \to K$
definida como o traço de $y \mapsto x \cdot y : L \to L$, considerado
como um $K$-endomorfismo de $L$ (endomorfismo como um espaço vetorial sobre $K$).
\begin{prop}[veja G1]
Pelo qualquer elemento $x \in L$ temos que
\begin{equation}
             \tr_{L/K} (x) = σ_1(x) + … + σ_n(x)
\end{equation}
Também se $P_x(y) = y^d - a_1 y^{d-1} + ...$ é um polinômio mínimo de $x$ sobre $K$,
então $\tr_{L/K} (x) = \frac{[L:K]}{\deg P_x} a_1$.
\end{prop}

\begin{cor}
Pelos $x,y \in L$ temos
\begin{equation} \label{eq:tr-sigma}
            \tr_{L/K} (xy) = \sum_{i=1}^n σ_i(x) σ_i(y)
\end{equation}
\end{cor}
Compondo a função $K$-linear $\tr_{L/K} : L \to K$
e uma aplicação bilinear de produto $L\times L \to L$
construmos uma função $(x,y) \mapsto \tr_{L/K} (xy) : L\times L \to K$,
qual é uma forma simétrica $K$-bilinear no $L$ (considerado como um espaço vetorial $n$-dimensional sobre $K$).
Sobre os corpos algebricamente fechados (e de característica não igual ao $2$)
as formas bilineares quadráticas podem ser diagonalizadas (em algumas bases).
Para nossa forma temos a seguinte $Ω$-base preferida em qual ela se diagonaliza.
Temos $L = K[y]/P$, logo $L_Ω := L\otimes_K Ω = Κ[y]/P \otimes_k Ω = Ω[y]/P$.
Sobre $Ω$ o polinômio $P$ se fatoriza $P(y) = \prod_{i=1}^n (y-σ_i(x))$
com $σ_i(x)$ distintos (então $(y-σ_i(x))$ coprimos),
logo pelo teorema chinês de resto o anel $L_Ω = Ω[y]/P$ é isomorfo ao produto (soma direta)
de aneis $Ω[y]/(y-σ_i(x)) \simeq Ω$, i.e. $L_Ω$ tem $n$ idempotentes ortogonais $f_1,...f_n$
($f_i^2 = f_i$ e $f_i f_j = 0$ para $i≠j$). Em base de idempotentes ortogonais
a matriz de Gram da forma bilinear $\tr_{L/K} (xy)$ é a matriz identidade,
i.e. se as coordenadas de $x\in L$ são $x_1,...,x_n \in Ω$ ($x = x_1 f_1 + ... + x_n f_n$)
e as coordenadas de $y\in L$ são $y_1,...,y_n$, logo
\begin{equation}
\tr_{L/K} (xy) = x_1 y_1 + ... + x_n y_n
\end{equation}
A base de idempotentes ortogonais está definida módulo uma permutação de elementos,
podemos enumerar $f_1,...,f_n$ em tal jeito que as coordenadas $x_i$ são exatamente as imagens
de $x$ com respeito de $n$ $K$-mergulhões $σ_1,...,σ_n: L \to Ω$, i.e. $x_i = σ_i(x)$.
Outro jeito de ver mesmo é unir todos os $K$-morfismos $σ_i : L \to Ω$
em um morfismo de $Κ$-álgebras $\overline{σ} : L \to Ω^{\oplus n}$
($\overline{σ}(x) := (σ_1(x),...,σ_n(x))$) e ver que ele dá
um isomorfismo de $Ω$-álgebras $L_Ω \to Ω^{\oplus n}$.

Seja $e_1,…,e_n$ uma $K$-base de $L$.

Associamos com $e$ uma matriz $M = M(e) \in Mat(n\times n, Ω)$
(a matriz das coordenadas de elementos da base $e_j$ na base de idempotentes $f_k$,
i.e. a matriz de troca entre as bases $e_j$ e $f_k$)
\begin{equation} \label{e:m}
       M_{k,j} = σ_k(e_j)
\end{equation}
e uma matriz $N = N(e) \in Mat(n\times n, K)$ (a matriz de Gram de forma bilinear em base $e_i$)
\begin{equation} \label{e:n}
       N_{i,j} = \tr_{L/K} (e_i e_j)
\end{equation}
A discussão acima sobre diagonalização da forma bilinear implica
\begin{prop}
Seja $M^t$ a matriz transposta. Temos a igualdade
\begin{equation}
     Μ^t M = N
\end{equation} 
\end{prop}
\begin{proof}
Explicitamente, usando a definição de produto de matrizes,
a definição de matriz transposta ${M^t}_{i,k} = M_{k,i}$,
a definição \eqref{e:m} de $M$, o fato que $σ_k$ é morfismo (logo $σ(xy) = σ(x)σ(y)$),
a \Cref{eq:tr-sigma}, a definição \eqref{e:n} de $N$
obtemos a sequência de igualdades:
$(M^t M)_{i,j} = \sum_{k=1}^n M_{k,i} M_{k,j} = \sum_{k=1}^n σ_k(e_i) σ_k(e_j)
= \sum_{k=1}^n σ_k(e_i e_j) = \tr_{L/K}(e_i e_j) = N_{i,j}$.
\end{proof}
\begin{cor}
Então
\begin{equation} \label{e:n=m2}
\det(N(e)) = \det(M(e))^2
\end{equation}
\end{cor}

As matrizes $M(e)$ e $N(e)$ e os seus determinantes dependem de uma base $e_1,...,e_n$, mas
\begin{prop}
A classe de $\det(N(e))$ em grupo quociente $K^*/(K^*)^2$ é independente de escolha de uma base $e$.
\end{prop}
\begin{proof}
Sejam $e_1,...,e_n$ e $f_1,...,f_n$ um par de bases de $L/K$ e $C(e,f)$ a matriz de troca,
i.e. $e_i = \sum_k c_{i,k} f_k$.
Então $\tr(e_i e_j) = \sum_{k,l} c_{i,k} c_{j,l} \tr(f_k f_l)$
(usamos linearidade de traço e bilinearidade de produto),
logo $N(e) = C(e,f)^t N(f) C(e,f)$
e 
\begin{equation} \label{e:n=nc2}
\det(N(e)) = \det(N(f)) \det(C(e,f))^2.
\end{equation}
\end{proof}
\begin{remark}
Extendendo as escalares até $Ω$,
a \Cref{e:n=m2} e o caso especial da \Cref{e:n=nc2} quando usamos como $f_1,...,f_n$ uma base
de idempotentes ortogonais: neste caso $C(e,f) = M(e)$ e $Ν(f)$ é uma matriz identidade (logo $\det N(f) = 1$).
\end{remark}

Finalmente vamos computar $\det M(e)$ por uma base especial.
Escolhemos um elemento primitivo $x$ de $L$ sobre $K$,
i.e. $L = K[x]/P$ por algum polinômio $P$ irredutível de grau $n$.
As raízes de $P$ são $n$ distintos (pela separabilidade) elementos de $Ω$
iguais $σ_1(x),…,σ_n(x)$ (por \Cref{p:raizes-mergulhações}).
Escolhemos uma base $1,x,x^2,...,x^{n-1}$ i.e. $e_i = x^{i-1},\, i = 1,…,n$.
Matriz $M(e)$ neste caso é igual à
\begin{equation}
     M_{i,j} = σ_i(x^{j-1}) = σ_i(x)^{j-1}
\end{equation}
(usamos que $σ(x^k) = σ(x)^k$),
i.e. $M$ é uma \emph{matriz de Vandermonde} para elementos $σ_1(x),…,σ_n(x) \in Ω$.
\begin{teorema}[Vandermonde. Lema 22.20 em AATA]
Sejam $a_1,...,a_n \in A$ elementos de um anel comutativo
e $V(a)_{i,j} = a_i^{j-1}$ uma matriz de Vandermonde.
Então
\begin{equation} \label{e:vdm}
\det V(a) = \prod_{i<j} (a_i-a_j) \in A
\end{equation}
\end{teorema}
\begin{proof}
Pelo qualquer $a_1,...,a_n \in A$ existe o único morfismo de anéis
$f_a : \Z[x_1,...,x_n] \to A$ tal que $f_a(x_i) = a_i$.
Pelo qualquer morfismo de anéis $f: A \to B$ temos
$V(f(a)) = f(V(a))$ e lado direito de equação também é $f$-imagem do lado direito.
Então basta provar só um caso $A = \Z[x_1,...,x_n], a_i = x_i$ no qual
$V$ é um polinômio $W \in \Z[x_1,...,x_n]$ homogêneo de grau $0+1+...+(n-1) = \frac{n(n-1)}{2} = \binom{n}{2}$.
Vejamos que a substituição $x_j \mapsto x_i$ (para $j≠i$) aniila $W$ ($W_{|_{x_j=x_i}} = 0$).
Logo $W$ é divisível por $x_i-x_j$. Os $\binom{n}{2}$ polinômios $x_i-x_j$ ($i<j$) são coprimos
e $\Z[x_1,...,x_n]$ é o domínio de fatoração única (pelo teorema de Gauss - veja
Corolário 2.4.8 no \CL ou Corolário 18.32 na AATA), logo
$W$ é divisível por o produto $\prod_{i<j} (x_i-x_j)$, mas o produto já é um polinômio
homogêneo de mesmo grau $\binom{n}{2}$, então $W = \alpha \prod_{i<j} (x_i-x_j)$ com $\alpha \in \Z$.
Para computar o constante $\alpha \in \Z$ basta de computar $W$ em um caso,
ou ver um coeficiente de $W$ e de produto (e.g. o coeficiente com respeito de monômio
$\prod x_i^{i-1}$).
\end{proof}

\begin{lema}
Seja $P \in K[x]$ um polinômio irredutível separável de grau $n$ e $L = L[x]/P$.
Pela qualquer $e_1,...,e_n$ de $L$ sobre $K$
a classe no grupo quociente $(K^*)/(K^*)^2$
de discriminante de uma base $e$ é igual à classe de discriminante de um polinômio $P$:
\begin{equation}
\det \tr_{L/K} (e_i e_j) = Discr(P) \in (K^*)/(K^*)^2
\end{equation}
Como um elemento de $K$ o discriminante de polinômio $P$ é igual
ao determinante de matriz de Gram em base de potências $1,x,x^2,...,x^{n-1}$.
\end{lema}
\begin{proof}
Pela \Cref{e:vdm} temos $\det(M) = \prod_{i<j} (σ_i(x) - σ_j(x))$.
Pela \Cref{e:n=m2} temos que $\det(N) = \det(M)^2$.
Logo $\det(N) = \det(M)^2 = \prod_{i<j} (σ_i(x) - σ_j(x))^2 = Discr(P)$.
\end{proof}

\end{document}
