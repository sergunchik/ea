/* Neste código usando extensões de Artin-Schreier em car. 2 e 5
   construimos um polinómio com grupo de Galois S_6 
   Usando método de Abel e teorema de Dedekind.
*/

/* Seja p - número primo, q = p^r uma potência de p.
   a \in \F_q --- um elemento de corpo dd q elementos (pode ser elemento de F_p)
   Um polinómio de Artin-Schreier é
    x^q - x - a
   Provamos que a aplicação y -> y^p - y é F_p-linear,
   se a na imagem o polinómio é fatorável,
   senão - ele é irredutível com grupo de Galois ≈ Z/p ger. pelo y -> y+1
*/

P7 = Mod( (x^2+x+1)*prod(n=1,4,x-n) , 7)

P5i = x^5 - x - 1
polisirreducible(P5i)
P5 = Mod( x * P5i , 5)

P = chinese(P7, P5)
Q = lift(P)
polisirreducible(Q)
polgalois(Q)

