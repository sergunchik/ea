Um jeitinho difícil de construir uma extensão de Galois
com grupo (Z/2)^2.

0. Pegamos k = C e K = k(z).

1. Consideramos 3 transformadas de Möbius

a:   z -> -z
b:   z -> 1/z
c:   z -> -1/z

Notamos que ab = ba = c, ac = ca = b, bc = cb = a,
equivalentemente a^2 = b^2 = c^2 = abc = 1,
então qualquer dois deles comutam e geram um subgrupo no PGL(2,k) isomorfo ao (Z/2)^2.

2. Temos um corpo de funções racionais K = k(z)
no qual nossas 3 involuções a,b,c agem.
Vamos computar os invariantes.

K^a = {f | f(z) = f(-z)} = {f| ∃g, f(z) = g(z^2) } =    k(u) \subset k(z) onde u -> z^2
K^b = {f | f(z) = f(1/z) } = {f : ∃h, f(z) = h(z+1/z)} =  k(v) \subset k(z) onde v -> z+1/z
K^c = {f | f(z) = f(-1/z) } = {f: ∃j, f(z) = j( i(z-1/z) ) } = k(w) \subset k(z) onde w -> i(z-1/z)

Vejamos que para computar K^A e K^B não usamos números especiais, mas para K^C precisamos i - um número tal que 1/i = -i (i.e. i^2 = -1).

3. Outro (mais geral e facil) jeito de computar invariantes é seguinte:
 função racional f de grau d é igual ao razão de 2 polinómios homopgêneos de grau d

f(z) = P(Z,W) / Q(Z,W) onde (z:1) = (Z:W).
Os polinómios P e Q são definidos módulo simultánea multiplicação pelo um constante em k^*.

Vamos tentar homogenizar as ações de a,b,c.

Como dado A é um elemento de um grupo PGL(2,k),
mas este grupo não age no espaço vetorial de polinómios homogêneos (so na projetivização dele),
o grupo que age no espaço vetorial de polinómios homopgêneos é GL(2,k),
temos uma sequência de grupos

1 -> k^* -> GL(2,k) -> PGL(2,k) -> 1
(i.e. PGL(2,k) = GL(2,k) / k^*)

Então pela definição de PGL como quociente de GL podemos leva tar qualquer elemento de PGL para GL, e temos mais de que 1 jeito (|k^*| jeitos) de fazer isso.
Escolhemos qualquer três levantamentos de a,b,c, e.g. 
A: (Z,W) -> (-Z , W)
B: (Z,W) -> ( W , Z)
C: (Z,W) -> (-W , Z)
Vejamos que A^2 (Z,W) = (Z,W) e B^2 (Z,W) = (Z,W) e C' = AB,
mas (C')^2 (Z,W) = (-Z,-W) ≠ (Z,W) e BA = (Z,W) -> (W,-Z) = -C' ≠ C'.



Notamos que se queremos salvar as relações a^2 = 1, normalmente
cada involução 'a' tem as duas levantamentos no GL(n,C) tais que
eles também são involuções em GL(n,C), A e -A:
se A é uma involução (Α^2 =1) e λ é um número,
então (λA)^2 = λAλA = λ^2 Α^2 = λ^2,
logo λA também é uma involução sse λ=1 ou λ=-1.
Similarmente se C é uma levantamento de uma involução c,
sabemos que imagem de C^2 em PGL é igual ao c^2=1,
então C^2 é uma matriz escalar: C^2 = μ,
logo (λC)^2 = λ^2 μ, então para obter involução precisamos
tomar uma raiz quadrátic de 1/μ (então precisamos que k é quadr. fechado e car≠2).
Então em vez de C' podemos tomar C que é uma involução (usando i t.q. i^2 = -1)
iC'=: C : (Z,W) -> (-iW,iZ)
Agora temos que AB = -i C e BA = i C,
A^2 = B^2 = C^2 = 1, ABC = BCA = CAB -i, ACB = CBA = BAC = i.

Exercício: mostrar que o subgrupo de GL(2,C) gerado pelo 
qualquer levantamentos de qualquer 2 involuções nunca é isomorfo ao (Z/2)^2.
Se alguns 2 levantamentos não comutam, também qualquer outras 2 levantamentos
não comutam, porque diferença entre levantamentos é um elemento escalar (logo central),
então o comutador de dois levantamentos é uma matriz central que não depende
de levantamentos escolhidos.

Um jeito comum e conveniente é escolher todos os três levantamentos A,B,C em SL(2,C):

A: (Z,W) -> (-iZ,iW)
B: (Z,W) -> (iW ,iZ)
C: (Z,W) -> ( W ,-Z)

Α^2 = Β^2 = Cˆ2 = -1 = ABC = BCA = CAB =
AB = C, BC = A, CA = B
BA = -A, CB = -A, AC = -B
ACB = CBA = BAC = 1

Neste caso o grupo gerado pelos A,B,C tem 8 elementos
1,-1,A,-A,B,-B,C,-C
e é o grupo de quatêrnios Q_8 (também o grupo de Heisenberg Hei_2).

O centro dele é o subgrupo de 2 elementos {1,-1},
e o quociente com respeito deste centro é o grupo (Z/2)^2 de {1,a,b,c| que começamos.

4. Se antes buscamos pelos funções racionais f invariantes
     f a = f
  agora podemos olhar pelo classe de polinómios homogêneos _quase_-invariantes,
  i.e. P A = λ(A) P
  onde λ(Α) é um constante.
  Função f = P/Q é invariante sse polinímios P e Q são quase-invariantes
  com o mesmo constante λ.
  P A = α P, Q A = α Q; P B = β P, Q B = β Q.
  Logo
  (-1)^d P = P (-1) = P A A = α P A = α^2 P, então α^2 = β^2 = (-1)^d  [ d = deg P ]
  P C = P A B = α P B = αβ P, Q C = αβ Q
  Seja γ = αβ, então PC = γP e como antes γ^2 = (-1)^d.
  Mas γ^2 = (αβ)^2, logo d tem que ser par!
  I.e.

  Então vejamos que uma função racional invariante com respeito de A
  pode ser uma razão de 2 polinômios homogeneous invariantes,
  ou também pode ser uma razão de 2 polinômios homogêneous anti-invariantes.
  Logo precisamos computar estes coisas.

  Vejamos  como A,B,C agem no espaço de polinómios de grau 2:


     |  A: (Z,W) -> (-iZ,iW)  |  B: (Z,W) -> (iW ,iZ)  |  C: (Z,W) -> ( W ,-Z)
------------------------------------------------------------------------------
Z^2  |        -Zˆ2            |           -W^2         |           W^2
ZW   |         ZW             |           -ZW          |          -ZW
W^2  |        -W^2            |           -Z^2         |           Z^2

então ZW, (Z^2+W^2), (Z^2-W^2) é uma base em qual A,B,C são diagonais simultaneamente
com autovalores

        |  A: (Z,W) -> (-iZ,iW)  |  B: (Z,W) -> (iW ,iZ)  |  C: (Z,W) -> ( W ,-Z)
------------------------------------------------------------------------------
  ZW    |           +1           |           -1           |          -1
Z^2+Wˆ2 |           -1           |           -1           |          +1
Z^2-W^2 |           -1           |           +1           |          -1

Similarmente produtos destes elementos são autovetores simultáneas nos
espaços de polinômios homogêneos de graus maiores.
Se denotamos S = ZW, T = Z^2 + W^2 e U = Z^2 - W^2
temos uma relação quadrática entre eles
       T^2 - U^2 = 4 S^2

Vejamos que U/S = (z-1/z) é C-invariante, T/S = (z+1/z) é B-invariante
e U/T = (z-1/z)/(z+1/z) = (z^2-1)/(z^2+1) é A-invariante.
U/T é transformação de Möbius de z^2.

Também vejamos que espaço 5-dimensional de pol. de grau 4
é decomposto como 5 = 2+1+1+1 com respeito de autovalores,
o espaço 2-dimensional é o espaço de invariantes - gerado pelos T^2,Uˆ2,S^2
com uma relação acima. Então o invariante é uma função de grau 4,
e.g. T^2/Sˆ2 = z^2 + 2 + 1/z^2, ou (T/S)^2 - 2 = z^2 + 1/z^2.


5. Na verdade é claro que aplicação z -> z^2 + 1/z^2
é invariante com respeito de grupo {1,a,b,c} e tem grau 4,
então se denotamos x = z^2 + 1/z^2, temos k(x) \subset k(z)
e essa é uma extensão de grau 4 normal com grupo de Galois (Z/2)^2.

6. Similarmente podemos construir funções racionais
associadas com outros sólidos platónicos.


7. Diedro: ζ - raiz primitiva n-ésima de 1.

    a : z -> ζ z
    b : z -> 1/z

    b a b = 1/a

    f = z^n + 1/z^n  é um gerador de invariantes.

8. Tetraedro e grupo Σ_4.

    Passo 1: acima temos   w = f(z) = z^2 + z-2 com grupo de Galois V_4 = (Z/2)^2
     Se p^2 + p^-2 = w, denotamos q = -p, r = 1/p, s = -1/p,
    então p,q,r,s são 4 raizes de z^4 - w z^2 + 1 com a ação de Klein
      a = (pq) (rs) ; b = (pr)(qs) ; c = (ps)(qr)

    Passo 2: consideramos pontos cricais de f:
              I. 0,∞ com valor critical ∞
             II. 1,-1 com valor critical +2
            III. i,-i com valor critical -2

    Passo 3. Dentro de grupo de Möbius de transformações em w
             consideramos subgrupo Σ(3) que permuta 3 pontos ∞,-2,2
                  e.g. w->-w                    é (2 -2)
                   b:  w -> (2w-12)/(w+2)       é (-2 ∞ 2)
                       etc
              O invariante para ação de Σ(3) no k(w)
               são gerados pela função de grau 6 g(w) = P^2/Q^3 (ou Q^3/P^2)
               onde P e Q são pol. homog. quase-inv de graus 3 e 2.
               Zeros de P são ∞,2,-2 i.e. P(W',W) = (W'-2W)(W'+2W)W
               Zeros de Q são 2 pontos fixos de b:
                          w = (2w-12)/(w+2) então
                          w^2 +2w = 2w - 12 i.e. w^2 - 12 = 0 logo Q = W'^2 - 12 W^2.
               Então t=g(w) é Möbius de (w^2-12)^3 / (w^2-4)^2

     Passo 4. Agora t=h(z) = g(f(z)) é uma função racional de grau 6*4 = 24
              que gera extensão normal de k(t) de grau 24 com grupo de Galois Σ(4)
           NB. Precisamos que k tem algumas raizes de unidade,
               como é comum em método de res. de eq. de grau 4.
         Exerc. achar polinómios homog. quase-invariantes de graus 4,6,4
        ( cujas raizes correspondem aos vertices, arestas e faces de tetrahedro).
         E.g. 6 arestas (3 pares) - 6 pontos critcais de f(z) = z^2 + z^{-2},
            outras - z-preimagens (f^{-1}(w)) de pontos criticais de w->g(w)

9. Icosaedro/dodecaedro e grupo Σ(5)

      Para construir ação de A_5 sobre k(z) podemos tentar
         de extender uma ação de A_4 sobre k(z) de acima pelo um 5-cíclo compatível.
      Se temos ação de A_5 no L = k(z), podemos considerar
         K = L^{A_4}, então L é extensão de grau 5 de K com grupo de Galois A_5.


     Exercicio. Achar polinómios homogêneos Z,X,Y A_5-invariantes de graus 12, 30, 20 
                (cujas raizes corr. aos vértices, arestas e faces de icosaedro).
                 Mostre que eles satisfazem uma relação
                       X^2 + Y^3 + Z^5 = 0

10. Também é possível reconstruir essas coisas começando com 12 pontos no P^1.
Um jeito de escolher estes 12 pontos é como:
0,∞ e 10 pontos do tipo ζ^a + ζ^b onde a≠b e ζ é uma raiz 5-ésima de 1.
Deste ponto de vista é fácil de ver um grupo dihedral,
gerado pelo rotação   z -> ζ z    e inversão z -> C/z pelo alguns valores de C.
Mas também é possível ver uma simetria de ordem 3
Dica : construi bijeção geometrica entre 12 pontos acima e 12 vertices de icosaedro,
escolha qualquer simetria de ordem 3 de icosaedro,
escolha qualquer órbita dessa simetria, construi a única transformada de Möbius
que permuta 3 pontos no Pˆ1 que correspondem aos 3 vértices de órbita.
