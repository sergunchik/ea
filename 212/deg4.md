\newcommand{\F}{\mathbf{F}}
\newcommand{\Z}{\mathbf{Z}}


Na aula passada (nov 23) com equação de grau 4
\begin{equation}
P = x^4 + p x^2 + q x + r  \in k[x]
\end{equation}
e 4 raizes $x_1,x_2,x_3,x_4$, i.e. 
\begin{equation}
P = (x-x_1)(x-x_2)(x-x_3)(x-x_4)
\end{equation}
a gente associou outros 3 números
\begin{equation}
y_i = \sum_{g \in \{e, (4i)(jk)\}}  g (x_1 x_2)
\end{equation}
onde $\{i,j,k,4\} = \{1,2,3,4\}$.
I.e. $i$ correspondem aos três subgrupos de grupo de Klein 
\begin{equation} V_4 = \{ e, (41)(23), (42)(13), (43)(12) \} \simeq (\Z/2)^2 \end{equation}
(então os três subgrupos correspondem aos três $\F_3$-pontos na reta projetiva $\P^1(\F_2)$.
Explicitamente
\begin{gather}
    y_1 = x_1 x_4 + x_2 x_3, \\
    y_2 = x_2 x_4 + x_1 x_3, \\
    y_3 = x_3 x_4 + x_1 x_2.
\end{gather}
Com estes três números associamos o polinômio
\begin{equation} Q = (y-y_1)(y-y_2)(y-y_3) \in k[y] \end{equation}

Para obter $x_1,…,x_4$ é conveniente considerar outra base
\begin{gather}
z_1 = y_2+y_3 = (x_1+x_4)(x_2+x_3) \\
z_2 = y_1+y_3 = (x_2+x_4)(x_1+x_3) \\
z_3 = y_1+y_2 = (x_3+x_4)(x_1+x_2) \\
Q’ = (z-z_1)(z-z_2)(z-z_3)
\end{gather}

Notamos que 
\begin{equation} z_1-z_2 = y_2-y_1 = (x_2-x_1)(x_4-x_3), \end{equation}
etc,
então os discriminantes dos três polinómios $P,Q,Q’$ são iguais.

Sabemos que 
\begin{equation}
  (x_1+x_4) + (x_2+x_3) = 0
\end{equation}
e
\begin{equation}
 (x_1+x_4) (x_2+x_3) = z_1
\end{equation}
então
\begin{equation}
(x_1+x_4)^2 = - z_1
\end{equation}
Similarmente
\begin{gather}
(x_1+x_3)^2 = - z_2, \\
(x_1+x_2)^2 = - z_3
\end{gather}

Então
\begin{equation}
x_1 %= x_1 + 0/2 &= x_1 + (x_1+x_2+x_3+x_4)/2 = \\
= \frac{ (x_1+x_2) + (x_1+x_3) + (x_1+x_4) }2  
= \frac{ \sqrt{-z_1} + \sqrt{-z_2} + \sqrt{-z_3} }2
\end{equation}
É importante notar que as três raizes de $-z_1,-z_2,-z_3$ não são independentes, mas o produto deles é fixo
(similarmente como em caso de cúbicas $u,v$ não são independentes, mas $uv = -p/3$).
