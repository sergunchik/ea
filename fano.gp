Estudantes = [Adriel, George, Marcio, Maria, Rafael, Thiago, Vinicius];
V = [ digits(n,2)[2..4] | n<-[9..15] ]

\\ [[0, 0, 1], [0, 1, 0], [0, 1, 1], [1, 0, 0], [1, 0, 1], [1, 1, 0], [1, 1, 1]]
#Estudantes == #V
M = apply(x->!x ,V~ * apply(V->V~,V) % 2)
