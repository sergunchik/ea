
\section{Módulos}

Os módulos sobre os anéis são os analogos das ações/representações de grupos,
mas no mundo dos anéis.
Essa estrutura algébrica generaliza ao mesmo tempo
\begin{itemize}
\item grupos abelianos,
\item espaçõs vetoriais sobre um corpo,
\item espaçõs vetoriais munidos com um endomorfismo,
\item ideais no anel.
\end{itemize}

Em particular neste curso a gente vai classificar os módulos finitamente gerados sobre os anéis euclideanos
(e mais geralmente sobre os anéis dos ideais principais),
obtendo a classificação dos grupos abelianos finitamente gerados
e a forma canônica de Jordan duma transformação linear como os corolários.

\begin{remark}
Para anéis não comutativos existem dois tipos de módulos: módulos esquerdos (categoria $A-Mod$) e módulos direitos (categoria $Mod-A$),
em jeito similar aos dois tipos de ações de um grupo não abeliano.
Similarmente com o caso dos ações de grupos abelianos,
dois tipos de módulos coincidem um doutro sobre os anéis comutativos.
\end{remark}

\begin{defin}[Módulos]
Seja $A$ um anel comutativo.
Um $A$-módulo (ou módulo sobre $A$)
é um grupo abeliano $M$ munido com um morfismo (de ação de $A$ no $M$)
\[ μ : A \times M \to M \]
i.e por cada $a\in A, m\in M$ imagem $μ(a,m)$ é denotado $a\cdot m$ (ou $am$),
tal que por cada $a\in A$
aplicação $μ_a : M \to M$ dado por $μ_a (m) = a\cdot m$
é um morfismo de grupos abelianos
e por quaisquer $a,b \in A$ temos
$μ_{ab} = μ_a \circ μ_b$,
i.e. por todos $m\in M$ temos $a\cdot(b\cdot m) = (a\cdot b)\cdot m$,
ou equivalentemente $μ(a,μ(b,m)) = μ(p(a,b),m)$ onde $p: A\times A \to A$
é a operação do produto no anel $A$.
\end{defin}

\begin{exem}
Se o anel é um corpo,
os seus módulos são espaços vetorias sobre ele.
\end{exem}

\begin{exem}
Cada anel pode ser considerado como um módulo sobre si mesmo.
\end{exem}

\begin{defin}[Morfismo de $A$-módulos]
Sejam $A$ um anel e $M,N$ dois $A$-módulos.
Um morfismo de $A$-módulos é um morfismo de grupos abelianos
$f: M\to N$ tal que $f(a\cdot m) = a\cdot f(m)$ por todos $a\in A$ e $m\in M$,
i.e. $μ_Ν \circ (Id_A\times f) = f \circ μ_M$ como duas funções
$A\times M \to N$.
\end{defin}

\begin{exer}
Definir submódulos. Verificar que por cada morfismo de $A$-módulos
$f: M \to N$ a imagem $Im(f)$ é um submódulo de $N$.
\end{exer}

\begin{defin}
Os submódulos do módulo trivial $A$ são chamados \emph{ideais} de anel $A$.
\end{defin}

\begin{exer}
Verificar que por cada morfismo de $A$-módulos
$f: M \to N$ o subconjunto de $M$ definido por
\begin{equation}
Núc(f) := \{ m\in M t.q. f(m) = 0 \}
\end{equation}
é um submódulo de $M$. Ele é chamado \emph{o núcleo} do morfismo $f$.
\end{exer}

\begin{exer}
Construir uma equivalência entre categorias de grupos abelianos e módulos sobre
o anel $\Z$ dos inteiros.
Isso é verifique que por cada grupo abeliano existe a única estrutura
de $\Z$-módulo e cada morfismo de grupos abelianos também é um morfismo
de $\Z$-módulos com respeito destas únicas estruturas de $\Z$-módulos.
\end{exer}

\begin{exer}
Seja $K[x]$ um anel de polinómios sobre um corpo $K$.
Construir uma equivalência entre a categoria de $K[x]$-módulos
e a categoria de pares $(V,\varphi \in \End_K(V))$
onde $V$ é um espaço vetorial $V$ sobre um corpo $K$
e $\varphi$ é uma autotransformação $K$-linear dele.
Morfismos na última categoria entre objetos $(U,\psi)$ e $(V,\varphi)$
são definidos como as transformações lineares $f : U \to V$
tais que $f\circ \psi = \varphi \circ f$.
\end{exer}

\begin{defin}[Geradores de módulo ou submódulo]
Dado uma função $γ : C\to M$, o conjunto imagem dos elementos de $A$-módulo $M$
é chamado \emph{um sistema de geradores} de $M$ se por qualquer $m\in M$
existe função $μ: C \to A$ tal que
$m = \sum_{c\in C} μ(c) \cdot γ(c)$.
Mais geral por qualquer função $γ : C\to M$
verifique que o conjunto dos elementos $\{\sum_{c\in C} μ(c) \cdot γ(c) | μ: C \to A$
é um submodulo de $M$. Ele é chamado submódulo gerado por $γ(C)$.
\end{defin}

\begin{defin}[Módulos cíclicos]
O $A$-módulo é chamado \emph{cíclico} se ele é gerado por um elemento.
Equivalentemente, existe o morfismo dos módulos $A\to M$ sobrejetivo.
\end{defin}

\begin{exer}
Provar que se $f: M \to N$ um morfismo de $A$-módulos sobrejetivo e $M$ é cíclico, então $N$ é cíclico.
\end{exer}

\begin{defin}
Por um $A$-módulo $M$
e um conjunto $X$ podemos munir o conjunto
$M^X$ das funções $f: X \to M$
com uma estrutura de $A$-módulo.
\end{defin}

\begin{exer} \label{mod-internal-hom}
Por qualquer dois $A$-módulos $M,N$ podemos munir
o conjunto dos morfismos de $A$-módulos $\Hom_{A-Mod}(M,N)$
com uma estrutura de $A$-módulo.
\end{exer}

\begin{exer}[Quociente de um módulo por um submódulo]
Seja $N \subset M$ é um submódulo do $A$-módulo $M$.
Provar que existe um $A$-módulo $Q$ e o morfismo de $A$-módulos
$q : M \to Q$ tal que $N$ é o núcleo de $q$ e por qualquer
morfismo de $A$-módulos $f : M \to L$ tal que $f(N) = 0$
existe o único morfismo de $A$-módulos $g: Q \to L$ tal que
$f = g \circ q$.
\end{exer}

\begin{defin}[Aplicação bilinear entre módulos]
Sejam $M,N,L$ três $A$-módulos.
Uma função $f : M\times N \to L$ de produto cartesiano
é chamada \emph{($A$-)linear em primeiro argumento}
se por cada $m\in M$ aplicação $f_m : N\to L$ dado por $f_m(n) := f(m,n)$
é o morfismo de $A$-módulos.
Similarmente ela é \emph{($A$-)linear em segundo argumento}
se por cada $n\in N$ aplicação $f_n : M\to L$ dado por $f_n(m) := f(m,n)$
é o morfismo de $A$-módulos. Função $f$ é chamada \emph{bilinear}
se ela é linear em ambos (primeiro e segundo) argumentos. 
\end{defin}

\begin{exer}
Definir soma das funções bilineares $f,g : M\times N \to L$
e produto $a \cdot f$ por qualquer $a\in A$.
Mostrar que o conjunto de todas funções bilineares de $M\times N$ para $L$
munido com soma, diferença, produto por escalares e função $0$ é um $A$-módulo.
\end{exer}

\begin{exer}[Produto tensorial de $A$-módulos]
Provar que por qualquer dois $A$-módulos $M,N$
existe a função ($A$-)bilinear universal, i.e.
existe o $A$-módulo $U$
munido com uma função bilinear $u: M\times N \to U$
tal que por qualquer função bilinear $f : M\times N \to L$ no $A$-módulo $L$
existe o único morfismo de $A$-módulos $g: U\to L$
tal que $f = g \circ u$.
\end{exer}

\begin{exer}
Sejam $u : M\times N \to U$ e $v : M\times N \to V$ duas funções bilineares universais.
Provar que existem os únicos morfismos de $A$-módulos $d: U \to V$ e $e: V\to U$
tais que $d\circ u = v$ e $e\circ v = u$.
Provar que $d\circ e = Id_V$ e $e\circ d = Id_U$.
\end{exer}


