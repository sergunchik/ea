% Coherence and Flatness. November 30, 2020. Algebraic Geometry.

On “coherence” and “flatness”.

Recall that an $A$-module $M$ is called \emph{coherent}
if it is finitely generated and finitely presented, i.e. there is an exact sequence

\begin{equation}
A^m \to A^n \to M \to 0
\end{equation}

with $n$ and $m$ finite. Here $A^n$ corresponds to the generators and $A^m$ to the relations.
In what follows we stick to Noetherian rings,
in this case coherence of modules is equivalent to finite generatedness.

\begin{definition} \label{def:coherent-sheaf}
For a scheme $X$ a sheaf of $\O_X$-modules $F$ is called \emph{coherent}
if for a covering by affines $U_i$ its sections $Γ(U_i,F)$ are coherent $Γ(U_i,\O_X)$-modules.
\end{definition}

\begin{remark}
There is a definition of coherence for sheaves over abstract ringed spaces,
which for schemes is equivalent to one above, but we won’t really need it.
You may want to see it later to make sense of coherence in analytic geometry,
e.g. in \href{https://en.wikipedia.org/wiki/Oka_coherence_theorem}{Oka coherence theorem}.
As far as I remember, a popular textbook of Harshorne just uses the definition I gave above.
\end{remark}

To define quasi-coherent sheaf --- allow above $n$ and $m$ to be infinite.

The categorical point of view on coherent sheaves is that they form an abelian category,
which is an “envelope” of a category of vector bundles:
morphisms of vector bundles are missing cokerels, so we just add them up formally.
Most basic
\begin{example}
The cokernel of a morphism of trivial line bundles,
corresponding to the morphism of cyclic $\C[x]$-modules $m_p : \C[x] \to \C[x]$
given by $m_x: y \mapsto (x-p)\cdot y$.
The cokerel is “skyscraper sheaf” in the point $x=p$ denoted as $\O_p$.
\end{example}


\subsection{Flatness.}


\begin{definition}
An $A$-module $M$ over a commutative ring $A$ is called \emph{flat}
if the tensor product
\begin{equation}
N \mapsto M \otimes_A N
\end{equation}
is a (left) exact functor.
\end{definition}


\begin{remark}
For any $A$-module $M$ the tensor product functor $N\mapsto N\otimes_A M$ is right-exact,
but for non-flat modules it is not left-exact.
\end{remark}

\begin{exercise}
Construct an example of a non-flat module over the ring of univariate polynomials $A = \C[x]$.
\end{exercise}

A result of commutative algebra guarantees that flatness is a local property:
\begin{theorem} \label{t:flat-localization}
For an $A$-module $M$ the following are equivalent
\begin{enumerate}
\item $M$ is a flat $A$-module
\item all localizations of $M$ are flat
\item for all maximal ideals $m\in A$ the localization $M_{m}$
of the module $M$ over the local ring $A_m$ of an ideal $m$ is flat
\end{enumerate}
\end{theorem}

\Cref{t:flat-localization} is used as a guide to define
\begin{definition} \label{d:flat-oxmodule}
For a (noetherian) scheme $X$ a sheaf of $\O_X$-modules $F$ 
over the sheaf of rings $\O_X$ is \emph{flat}
if locally $F$ is isomorphic to the sheaves of sections of \emph{flat}
$Γ(U,\O_X)$-modules for a covering of $X$ by open affine subsets $U$.
Equivalently for any closed point $p\in X$ consider an open affine $U \ni \p$
and let $\O_{X,p}$ be the localization of $Γ(U,\O_X)$ in the maximal ideal
corresponding to the closed point $p$ and $F_{X,p}$ the respective localization
of $Γ(U,F)$ (the stalk of sheaf of $\O_X$-modules over $p$).
Then the flatness of $\O_X$-module $F$
is equivalent to the flatness of 
$\O_{X,p}$-modules $F_{X,p}$ for all closed points $p\in X$.
\end{definition}

\begin{definition} \label{d:flat-morphism}
Recall that a homomorphism of rings $g : B \to A$
equips the ring $A$ with the structure of a $B$-module (and $B$-algebra),
and the morphism $g$ is called \emph{flat} if $A$ is flat as a $B$-module.

Similarly, a morphism  of schemes  $f : X \to Y$    is called   \emph{flat}
if the pushforward $f_* \O_X$ of the structure sheaf is a flat $\O_Y$-module.
\end{definition}

Flatness of morphisms is used in algebraic geometry
to make sense of speaking about “families” of varieties.
Usually varieties/schemes $X_y$ parametrized by $y\in Y$ are considered to be in a “family”
when they are fibers of a flat morphism $f : X \to Y$.

\begin{exercise}
Prove that the closed embedding of a point into an affine line is NOT flat.
\end{exercise}
\begin{exercise}
Prove that the blowup of a point on a plane is NOT a flat morphism.
I mean a map $((x,y),(X:Y)) \mapsto (x,y)$ from
$\{ (x,y) \in V, (X:Y) \in \P(V) :  xY = yX \}$ to $V = \A^2$ that forgets the line $(X:Y)$.
\end{exercise}
\begin{exercise}
Prove that the projection from a locally trivial bundle to the base is flat.
\end{exercise}


Similarly for sheaves $F$ on $X$  we want to introduce a notion of
\emph{relative flatness} over $Y$.
Note that push-forward $f_* F$ of a sheaf $F$ is a sheaf of
$f_* \O_X$-modules, but not of $\O_Y$-modules.
So to define a push-foward for (quasi)-coherent sheaves
we want to restrict scalars after the standard pushfoward of sheaves.
Locally, we have a morphism of rings $g : B \to A$
and an $A$-module $M$ that we can consider as a $B$-module thanks to $g$.


