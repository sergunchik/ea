
<https://ncatlab.org/nlab/show/Introduction+to+Topology>

0. Livros que combinam introdução à topologia geral com introdução a topologia algébrica.
 - O. Ya. Viro, O. A. Ivanov, N. Yu. Netsvetaev, V. M. Kharlamov : [Elementary Topology, Problem Textbook](http://pages.cs.wisc.edu/~dluu/ps/Viro,%20Ivanov,%20Kharlamov%20and%20Netsvetaev%20-%20Topology%20with%20problems%20and%20solutions.pdf)

1. Introdução a Topologia Geral. 
  - Steven Clontz: [Intro to Topology](https://stevenclontz.github.io/intro-to-topology)
  - [Allen Hatcher] : [Notes on Introductory Point-Set Topology](https://pi.math.cornell.edu/~hatcher/Top/Topdownloads.html)
  - Tai-Danae Bradley, Tyler Bryson, and John Terilla: [Topology. A Categorical Approach](https://topology.mitpress.mit.edu)
  - <https://ncatlab.org/nlab/show/Introduction+to+Topology+--+1>
  - Steven Vickers: Topology via Logic, Cambridge University Press (1989).

2. Grupo fundamental e cobrimentos.
  - [William Massey] : Algebraic topology: an introduction. 1967 (v7 - 1989)
  - [William Massey] : A Basic Course in Algebraic Topology, cap. I-V. 1980 (v3 - 1997)
  - [Tammo tom Dieck] : [Algebraic Topology](https://www.maths.ed.ac.uk/~v1ranick/papers/diecktop.pdf), cap. 1-3.
  - [Rotman] :
  - [Peter May] : [A Concise Course in Algebraic Topology](http://www.math.uchicago.edu/~may/CONCISE/ConciseRevised.pdf), 1999, cap. 1-4.
  - [Allen Hatcher] : [Algebraic Topology](https://pi.math.cornell.edu/~hatcher/AT/ATpage.html), 2002, cap 1.
  - <https://ncatlab.org/nlab/show/Introduction+to+Topology+--+2>
  - Jesper Moller: [The fundamental group and covering spaces](http://web.math.ku.dk/~moller/f03/algtop/notes/covering.pdf)
  - Clara Löh: [Algebraic topology, introductory course](http://www.mathematik.uni-regensburg.de/loeh/teaching/topologie1_ws1819/lecture_notes.pdf), ap. A.1 + cap. 0-2.

[William Massey]: https://en.wikipedia.org/wiki/William_S._Massey
[Tammo tom Dieck]: https://pt.wikipedia.org/wiki/Tammo_tom_Dieck
