# Introdução a topologia ([MAT2714]/[MAT1702])

- Espaços métricos, espaços topológicos e continuidade.
- Espaços conexos e compactos.
- Classificação de superfícies.
- Grupo fundamental e aplicações.
- Espaços de recobrimento.

## Bibliografia
- William S. Massey.
  - Algebraic topology: an introduction. New York: Harcourt, Brace & World, 1967. ... seventh edition, Springer, 1989
  - A Basic Course in Algebraic Topology. 1980 ... third edition, Springer, 1997.
- Elon Lages Lima. Elementos de Topologia Geral; Rio de Janeiro: Projeto Euclides IMPA, 1976.
- Elon Lages Lima. Grupo Fundamental e Espaço de Recobrimento; Rio de Janeiro: Projeto Euclides IMPA, 1993.

### Bibliografia Complementar
- BOAS, R. P. A Primer of real functions. 2. ed; Buffalo: Mathematical Association of America, 1972.
- ÁVILA, G. Introdução à Análise Matemática; São Paulo: Ed. Edgard Blucher, 1999.
- Elon Lages Lima. Curso de Análise. Vol. I; Rio de Janeiro: Projeto Euclides IMPA, 1995.
- Elon Lages Lima. Curso de Análise. Vol. II; Rio de Janeiro: Projeto Euclides IMPA, 2000.
- Elon Lages Lima. Espaços métricos; Rio de Janeiro: IMPA; Brasília, DF: CNPq, 1977.

## Pré-requisitos (um de pares)
- [MAT1153] e [MAT1604]
- [MAT1153] e [MAT1605]
- [MAT1163] e [MAT1605]
- [MAT1173] e [MAT1604]
- [MAT1173] e [MAT1605]
- [MAT1183] e [MAT1605]
- [MAT1604] e [MAT1605]

[MAT1153]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1153
[MAT1163]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1163
[MAT1173]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1173
[MAT1183]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1183
[MAT1604]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1604
[MAT1605]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1605
[MAT1702]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT1702
[MAT2714]: https://www.puc-rio.br/ferramentas/ementas/ementa.aspx?cd=MAT2714
