Vamos discutir o **teorema sobre levantamento de caminhos**
 e os seus aplicações - a aplicação principal é o teorema sobre equivalência
da categoria 
$$R_X = Recobrimentos(X)$$
de recobrimentos 
e da categoria 
$$Ações(πX)$$
de ações $ρ : π X \to Sets$ do grupoide fundamental $π X$.

A categoria $R_X$ é uma subcategoria completa da categoria relativa $Top_X$
onde restringimos a classe de objetos somente para recobrimentos.
Complete significa que por qualquer par de objetos $p,p'$ de $R_X$ temos
a identificação de morfismos em $R_X$ e em $Top_X$: $R_X(p,p') = Top_X(p,p')$.

- Objetos de $R_X$ são os recobrimentos $p: E \to Χ$.
- Um morfismo entre um par de objetos $p : E\to X$ e $p' : E'\to X$
é uma função contínua $f: E\to E'$ tal que $p = p' f$:
$$Top_X(p:E\to X,p':E'\to X) = \{ f\in Top(E,E') t.q. p = p' f\}.$$
- As identidades $Id_p \in Top_X(p,p)$ são dados por identidades $Id_E \in Top(E,E)$,
claro que $p = p \circ Id_E$ por qualquer $p: E\to B$.
- A lei da composição também está induzida das composições em $Top$:
só verifique que $p = p' f$ e $p' = p''g$ implica que $p = (p'' g)f = p'' (gf)$.



Também podemos levantar mais um nível, e ver que a associação
$$X \to R_X$$
é um funtor
$$Hot \to Cats$$
e a associação
$$X \to Cats(πX, Sets)$$
também é um funtor
$Hot \to Cats$
entre as mesmas categorias,
(que associa ao um espaço topológico X uma categoria de funtores ρ : π X -> Sets, etc)


Então a associação 
$$γ_X : R_X \to Cats(πΧ,Sets)$$
é uma transformação natural.
