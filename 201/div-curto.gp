arredondamento(z) = round(real(z))+I*round(imag(z));
resto(a,b) = a-b*arredondamento(a/b);
mdc(a,b) = if(!b,a,norm(b)>norm(a),mdc(b,a),mdc(b,resto(a,b)));
mdc(5,3+4*I)
mdc(50,30+4*I)
