



\\ Piso (parte inteiro de baixo) de número gaussiano
myfloor(z) = (floor(real(z))+I*floor(imag(z)));

/* 
Dividir um número gaussiano (a) doutro (b)  com resíduo 
Output é um vetor [d,r] t.q. a = b*d + r e norm(r) < norm(b) se b!=0.
*/
mydiv(a,b) =
{
z = a/b;
d = myfloor(z); /* EXPLIQUE pq este é errado e precisamos arredondamento em vez de piso!!! */
r = a-b*d;
return([d,r]);
}

mydiv2(a,b) =
{
if(norm(a)<norm(b),return([0,a]));
v = [1,-1,I,-I];
/* x vai ser um dos a-b, a+b, a-b*I, a+b*I */
for(n=1,#v,x=a-b*v[n];if(norm(x)<norm(a),return([v[n],0]+mydiv2(x,b))));
return("Erro: todos os vizinhos têm norma grande!"); /* Este coisa não vai aparecer */ 
}

/* Um jeito de computar maior divisor comum, dado algoritmo de divisão com resto */
mygcd(a,b) =
{
if(norm(b)>norm(a),return(mygcd(b,a)));
if(b==0,return(a));
return(mygcd(b,mydiv(a,b)[2]));
}
