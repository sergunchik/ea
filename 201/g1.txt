
Problema 1:	Seja A um anel finito. Mostrar que 
(i) A é isomorfo ao produto dos anéis A_p, 
por p in 2,3,5,7,11,... t.q. ordem de A_p é um poder de p
(ii) esta fatoração de A é única.

Problema 2: Provar que DFUs são inteiramente fechados.

[mais info no PDF de EA-17, maio 21]


Problema 3: provar que o polinómio P é irredutivel no Z[x]

P_1	= x^6 + x^3 + 1
P_2	= x^6 + x^2 + 1
P_3	= x^6 + x + 1
P_4  	= x^6 + x^5 + x^3 + x^2 + 1
P_5    	= x^6 + x^5 + x^4 + x^2 + 1
P_6   	= x^6 + x^2 + x + 1


Problema 4. Verificar que polinómio Q não tem raizes racionais,
mas tem raizes modulo cada número primo.

Q_1 	= x^6 + 7 x^4 - 36
Q_2 	= x^6 + x^4 - 4 x^2 - 4
Q_3 	= x^6 + x^4 - 9 x^2 - 9
Q_4 	= x^6 - 11 x^4 + 36 x^2 - 36
Q_5 	= x^6 - x^4 - 24 x^2 - 36
Q_6 	= x^6 + 5 x^4 - 12 x^2 - 36

Dica - usar Lema do março 12 (aula 2)


Problema 5: computar mdc(a,b) para dois números de Eisenstein Z[w]/(w^2+w+1 = 0)

A_1	= mdc(19w+11, 9w+10)
A_2	= mdc(19w-2, 14w+17)
A_3	= mdc( 8w+3,  8w+17)
A_4	= mdc(26w-1, 29w+43)
A_5	= mdc( -w-31,13w-38)
A_6	= mdc(20w+54,37w+44)

Problema 6:  quantos divisores de zero tem o anel Z/NZ por

N_1	= 196883
N_2	= 220520
N_3	= 1000000
N_4	= 22451900
N_5	= 744744
N_6	= 510510

Dica 1: provar que no anel finito cada elemento é inversivel ou divisor de 0. cf. demonstr. que domínios finitos são corpos.Dica 2: ver aula 3, março 24. teorema chinês de resto, função de Euler.


Problema 7. Mostre que por cada número primo p e par dos polinómios R,S no (Z/p)[x]
R(y) = S(y) por y=0,1,2,...,p-1
se e so se
polinómio (x^p-x) é um divisor de (R-S).

Dica: considerar raizes de xˆp-x, lembra Fermat, Frobenius, ...


Problema 8.

1	I.   Exploração 2.3.1
2	II.  Exploração 2.3.2
3	III. Provar que anel de Explorações 2.3.1/2.3.2 é um dominio
	     Dica: o que é corpo dos frações? cf. aulas 12-13


Problema 9.

1	4.1.11	Se cada ideal no anel é finitamente gerado, o anel é noetheriano.
2	4.1.9	Cada ideal num anel noetheriano é finitamente gerado
3	4.1.10	Se I_n é uma cadeia ascendende dos ideais num anel, união dos I_n é um ideal



Problema 10.

1	3.2.8 	
2	4.2.13 	No DIP cada ideal primo é maximal
3	3.2.6



B1. Seja p um número primo.
Provar que as seguintes são equivalerntes

(i)	p=1 (8) ou p=3(8)
(ii)	p = aˆ2 + 2 bˆ2 	por alguns inteiros a,b

NB - este problema tinha errado 3 b^2 em vez de 2 b^2!!



B2. Munir 8 artigos de lingua portuguesa
	um,uns,uma,umas,o,os,a,as
com operação 'mais':
um mais um	= uns
uma mais uma	= umas
um + umas	= uns
o + o = os
o + a = os
a + as = as
umas + as = umas
etc
o + um = os + um = o + uns = os + uns = o + uma = os + uma = o + umas = os + umas 

(i) Mostrar que este é um semi-grupo
(ii) Construir um ideal no algebra deste semi-grupo   [Dica - considerar artigos definidos ou indefinidos ]
(iii) Diz se existe um monomorfismo deste semi-grupo para monoide multiplicativo dos residuos Z/NZ por algun N.

vim:tw=78:sw=2:ts=2:ft=help:norl:nowrap:
