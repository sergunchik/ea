# Estruturas Algébricas

- Parte  I: grupos e anéis, morfismos e quocientes
- Parte II: extensões de corpos e teoria de Galois

- 201/    2020.1, Estruturas Algébricas I
- 202/    2020.2, Estruturas Algébricas II
- 212/    2021.2, Estruturas Algébricas II

# Outros cursos:

- ac/     Álgebra Comutativa (2020.1)
- ag/     Geometria Algébrica (2020.2)
- lean/   Lógica, provas e seus assistentes (2021.2)
- top/    Introdução a topologia (2022.1)

# Cursos de futuro:
cat/    Teoria das categorias

