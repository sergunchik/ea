P = List();
Estudantes = [Adriel, George, Marcio, Maria, Rafael, Thiago, Vinicius,Adrielle]~;
\\listput(~P,Estudantes);
listput(~P,vectorv(8,n,"S1:"));
listput(~P,[   /* Exercícios de 1.3 (conjuntos) */\
  4,11,16;    /* Adriel */\
  8,14,15;    /* George Lucas */\
  6, 7,27;    /* Márcio */\
  4,13,26;    /* Maria Clara */\
  3,14,23;    /* Rafael */\
  6,10,21;    /* Thiago */\
  5, 9,12;    /* Vinicius */\
 10,12,23])   /* Adrielle */
\\+3 para todos: 19, 28, 29
listput(~P,vectorv(8,n,". S2:"));
listput(~P,[   /* Exercícios de 2.3 (inteiros) */\
  2, 10, 20, 28;    /* Adriel */\
  3, 11, 21, 29;    /* George Lucas */\
  4, 12, 22, 30;    /* Márcio */\
  5, 13, 23, 31;    /* Maria Clara */\
  8, 14, 23, 24;    /* Rafael */\
  9, 16, 22, 25;    /* Thiago */\
  8, 13, 18, 26;    /* Vinicius */\
 12, 19, 27, 28])   /* Adrielle */
listput(~P,vectorv(8,n,". S3:"));
E3 = [   /* Exercícios de 3.3 (grupos) */\
25,36,47, 5,16,27,38,49, 7,18,29,40,51, 9,20;\
32,43,54,12,23,34,45, 3,14,25,36,47, 5,16,27;\
39,50, 8,19,30,41,52,10,21,32,43,54,12,23,34;\
46, 4,15,26,37,48, 6,17,28,39,50, 8,19,30,41;\
53,11,22,33,44, 2,13,24,35,46, 4,15,26,37,48;\
7,18,29,40,51, 9,20,31,42,53,11,22,33,44, 2;\
14,25,36,47, 5,16,27,38,49, 7,18,29,40,51, 9;\
18,29,40,51, 9,20,31,42,53,11,22,33,44, 2,13];
listput(~P,extract(E3,vector(5,n,3*n)));
