\section{Ações de grupos nos conjuntos.
$G$-equivalência, órbitas, estabilizadores, conjunto quociente.}

\begin{defin}
\emph{Uma ação} (de esquerda) $G:C$ dum grupo $G$ no conjunto $C$ 
é um morfismo de conjuntos
\[ a: G\times C \to C \]
tal que para todos $g,h\in G$ e $c\in C$ temos
$a(gh, c) = a(g,a(h,c))$ e $a(e_G,c) = c$.
\end{defin}
\begin{prop}
Equivalente, uma ação $G:C$ 
é um morfismo de grupos
\[\alpha: G\to \Aut_{Conjuntos} C \]
Isso é, por cada $g\in G$ associamos um automorfismo $\alpha_g$ de $C$
em tal modo que por todos $g,h\in G$ temos
$\alpha_{gh} = \alpha_g \circ \alpha_h$ e $\alpha_{e_G} = Id_C$.
\end{prop}
\begin{proof}
$\alpha(g)(c) = a(g,c)$
\footnote{Na programação este truque é chamado \emph{currying},
na matemática podemos escrever $\Hom(A\times B, C) = \Hom(A,\Hom(B,C))$.}
\end{proof}
Em seguinte se a ação é claro de contexto podemos escrever
$g\cdot c$ ou simplesmente $gc$ em vez de $\alpha_g c$ ou $a(g,c)$.

\begin{defin}
Por um grupo fixo $G$ um $G$-conjunto é um conjunto $C$ munido com $G$-ação $G:C$.
Um morfismo de $G$-conjuntos $G:C$ e $G:D$ é um morfismo de conjuntos $f: C\to D$
tal que por todos $c\in C, g\in G$ temos $f(gc) = g f(c)$.
Morfismo é um isomorfismo se ele tem o inverso.
Dois $G$-conjuntos são isomorfos se existe um isomorfismo entre eles.
\end{defin}

\begin{exem}[Ação regular]
Um grupo $G$ age na si mesmo de multiplicação esquerda:
\[ a_L(g,h) = g\cdot h \]
\[ L_g (h) = g\cdot h \]
Aqui $g\cdot h$ significa produto no grupo.
\end{exem}
\begin{proof}
As axiomas de ação aplicando neste caso ficam exatamente
as axiomas de associatividade e elemento neutro de grupo $G$.
\end{proof}

\begin{teorema}[Cayley]
Cada grupo é isomorfo ao subgrupo do grupo das permutações.
\end{teorema}
\begin{proof}
Ação regular considerado como um morfismo de grupos
\[ L : G \to \Aut_{Conjuntos} (G) \]
é injetivo: se por qualquer $g\in G$ temos $L_g = Id_G$ então
$g = g\cdot e = L_g (e) = Id_G(e) = e$.
\end{proof}

\begin{exem}
Podemos definir uma ação (esquerda) regular usando multiplicação de direita:
\[ R_g (h) := h \cdot g^{-1} \]
\end{exem}

\begin{exer}
Ações $L$ e $R$ são isomorfos.
\end{exer}
\begin{proof}
Aplicação dos conjuntos $g\to g^{-1}$ é um isomorfismo de ações.
\end{proof}

\begin{exem}
\emph{Ação adjunta} (ou ação de conjugação) $G:G$ é definida por
\[ Ad_g (h) := g h g^{-1} \]
É uma restrição de ação bidirecional $G\times G: G$ $(l,r)\cdot h := l h r^{-1}$
para a diagonal $(g,g) \in G\times G$.
\end{exem}

\begin{prop}
Ação adjunta é uma ação no grupo, que significa
\[ Ad_g (h_1 \cdot h_2) = Ad_g (h_1) \cdot Ad_g(h_2) \]
e $Ad_g(h^{-1}) = {(Ad_g(h))}^{-1}$, $Ad_g e_G = e_G$.
Em particular imagem $Ad_g (H)$ dum subgrupo $H\subset G$ é um subgrupo.
\end{prop}

\begin{prop} \label{rel}
Por ação $G:C$ uma relação no $C$ definido por
\[ c \sim d \iff \exists\,g\in G\, | gc = d \]
é uma relação de equivalência.
\end{prop}

\begin{defin}
As classes de equivalência com respeito de relação de \Cref{rel}
são chamados \emph{as órbitas}.
Por um elemento $c\in C$ a órbita dele é um conjunto
\[ G c := G\cdot c := \{g\cdot c\,|\,g\in G\} = \{d\in C\,|\,\exists g\in G\, d=gc\} \]
\end{defin}

\begin{exem}
Se $H\subset G$ é um subgrupo podemos restringir ação regular direito $G:G$
para $H:G$. O quociente é isomorfo ao conjunto-quociente das classes laterais de direita
$G/H = \{gH\}$. Ele ainda tem uma estrutura de $G$-conjunto com respeito
de multiplicação de esquerda
\[ h\cdot (gH) = (hg) H \]  
\end{exem}

\begin{defin}
Quociente ($G\backslash C$ ou $C/G$)
dum $G$-conjunto $C$ com respeito de ação é um conjunto das órbitas.
Normalmente $C/G$ é usado para ações direitas e $G\backslash C$ para ações esquerdas.
\end{defin}

\begin{defin} \label{d:ação-transitiva}
A ação $G:X$ é chamada \emph{transitiva} se
$\forall x,y\in X \exists g\in G : gx = y$ i.e. ela tem só uma órbita.
As vezes adicionalmente pedirmos que $X$ não é vazio.
\end{defin}

\begin{defin} \label{d:ação-efetiva}
O núcleo da ação $G:X$ é o núcleo do morfismo $G\to \Aut X$,
i.e. $\{g\in G : \forall x\in X, gx = x\}$.
A ação é chamada \emph{efetiva} se ela não tem núcleo,
i.e. $\forall g\neq e\in G \exists x\in X : gx \neq x$.
\end{defin}

\begin{exer}
Provar que por qualquer duas ações efetivas e transitivas
existe um isomorfismo entre os respetivos $G$-conjuntos.
\end{exer}

\begin{prop}
Um $G$-conjunto $C$ é união disjunto das órbitas:
\[ C = \bigcup_i O_i \]
Se um conjunto $C$ é finito cardinalidade dele é soma das cardinalidades das órbitas
\[ |C| = \sum_i |O_i| \]
\end{prop}

\begin{defin}
Para ação $G:C$ \emph{o estabilizador} dum elemento $c\in C$ é definido como
\[ G_c := Stab(c) := \{g\in G\,|\, gc=c\} \]
\end{defin}

\begin{prop}[Teorema sobre as órbitas e os estabilizadores]
Cada estabilizador $G_c$ é um subgrupo de $G$, e cada órbita $G\cdot c$
é um $G$-subconjunto isomorfo como $G$-conjunto ao quociente $G/G_c$.
\end{prop}

